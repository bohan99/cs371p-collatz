#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
using namespace std;

// Storage array
long int storage[1000000];


pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// Calulates the cycle length
int calcMC(long int n) {
    // Base case, we already know the answer
    if (n < 1000000 && storage[n] != 0) {
        return storage[n];
    }
    // Base case, we reached 1
    if (n == 1) {
        storage[1] = 1;
        return 1;
    }
    // Calculates and stores the cycle length
    if (n < 1000000) {
        long int prev = n;
        if (n % 2 != 0) { // If odd
            n = n + (n >> 1) + 1;
            storage[prev] = 2 + calcMC(n);
        } else { // n is even
            n /= 2;
            storage[prev] = 1 + calcMC(n);
        }
        return storage[prev];
    } else {
        if (n % 2 != 0) {
            n = n + (n >> 1) + 1;
            return 2 + calcMC(n);
        } else {
            n /= 2;
            return 1 + calcMC(n);
        }
    }
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    // Creates a temp max
    long int max = 0;
    // If i > j, swap
    if (i > j) {
        int temp = j;
        j = i;
        i = temp;
    }
    // Optimize in that if i is less than half of j,
    // start from half of j + 1
    if (i <= j / 2) {
        i = (j / 2) + 1;
    }
    // Loop through all numbers between i and j
    for (long int index = i; index <= j; ++index) {
        long int indiv = 0;
        if (index > 1000000 || storage[index] == 0) { // If we don't have the data
            // Calculates and stores the result
            indiv = calcMC(index);
            if (index < 1000000 && index > 0) {
                storage[index] = indiv;
            }
         } else { // Reads in the data
            indiv = storage[index];
        }
        // If the current one is bigger than max
        if (indiv > max) {
            max = indiv;
        }
    }
    return max;
}



// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}