// ------------------------------------
// projects/c++/collatz/TestCollatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// ------------------------------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);}

// My own unit tests
TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(1, 1000);
    ASSERT_EQ(v, 179);}
TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(12, 31232);
    ASSERT_EQ(v, 308);}
TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(846, 1246);
    ASSERT_EQ(v, 182);}
TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(34237, 104813);
    ASSERT_EQ(v, 351);}
TEST(CollatzFixture, eval_9) {
    const int v = collatz_eval(19, 21);
    ASSERT_EQ(v, 21);}
TEST(CollatzFixture, eval_10) {
    const int v = collatz_eval(123, 9840);
    ASSERT_EQ(v, 262);}
TEST(CollatzFixture, eval_11) {
    const int v = collatz_eval(29, 82916);
    ASSERT_EQ(v, 351);}
TEST(CollatzFixture, eval_12) {
    const int v = collatz_eval(826, 1928);
    ASSERT_EQ(v, 182);}
TEST(CollatzFixture, eval_13) {
    const int v = collatz_eval(91, 28461);
    ASSERT_EQ(v, 308);}
TEST(CollatzFixture, eval_14) {
    const int v = collatz_eval(102, 472);
    ASSERT_EQ(v, 144);}
TEST(CollatzFixture, eval_15) {
    const int v = collatz_eval(1932, 128461);
    ASSERT_EQ(v, 354);}
TEST(CollatzFixture, eval_16) {
    const int v = collatz_eval(9271, 284610);
    ASSERT_EQ(v, 443);}
TEST(CollatzFixture, eval_17) {
    const int v = collatz_eval(3746, 34656);
    ASSERT_EQ(v, 311);}
TEST(CollatzFixture, eval_18) {
    const int v = collatz_eval(20, 60);
    ASSERT_EQ(v, 113);}
TEST(CollatzFixture, eval_19) {
    const int v = collatz_eval(29, 47);
    ASSERT_EQ(v, 110);}
// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());}